﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmGame
{
    class Program
    {
        static void Main(string[] args)
        {
            bool turnos=true;
            float dinero;
            string decision;
            int decisionN;
            int expansion=10;
            int expansiones = 1;
            //Granja
            List<Plantas> granjaP = new List<Plantas>();
            List<Animales> granjaA = new List<Animales>();

            //Tienda
            List<Plantas> plantas = new List<Plantas>();
            List<Animales> animales = new List<Animales>();

            animales.Add(new Animales("Cerdo",3,30,1,true,true));
            animales.Add(new Animales("Vaca", 3,25, 1, true,true));
            animales.Add(new Animales("Oveja", 3,20, 1, true,true));
            animales.Add(new Animales("Gallina", 3,30, 1, true,true));

            plantas.Add(new Plantas("Papa", 6, 2,25,50,false,true));
            plantas.Add(new Plantas("Manzanal",4, 5,10,15, false,true));
            plantas.Add(new Plantas("Trigo", 9, 1,15,30, false,true));
            plantas.Add(new Plantas("Zanahorias", 6, 1,30,60,false,true));

            Console.WriteLine("Bienvenido a nuestro juego de granja. Primero, introduzca tu dinero inicial: ");
            dinero = Int32.Parse(Console.ReadLine());

            while (turnos==true)
            {
                Console.WriteLine("Buen día. ¿Qué desea hacer?");
                Console.WriteLine("Comprar Animal(1)");
                Console.WriteLine("Comprar Plantas(2)");
                Console.WriteLine("Expandir Terreno(3)");
                Console.WriteLine("Recolectar(4)");
                Console.WriteLine("Pasar turno(5)");
                decision = Console.ReadLine();
                if (decision == "1")
                {
                    Console.WriteLine("Estos son los animales a la venta");
                    {
                        foreach (Animales animal in animales)
                        {
                            Console.WriteLine(animal.Informacion());
                        }
                        Console.WriteLine("Seleccione uno: (6,7,8,9)");
                        foreach (Animales animal in animales)
                        {
                            if (dinero >= animal.Coste())
                            {
                                decision = Console.ReadLine();
                                decisionN = Int32.Parse(decision);
                                dinero = dinero - animal.Coste();
                                granjaA.Add(animales[decisionN - 6]);
                            }
                            else
                            {
                                Console.WriteLine("No tiene suficiente dinero, lo sentimos");
                            }
                            Console.WriteLine("Gracias por su compra");
                        }
                    }
                }
                else if (decision == "2")
                {
                    Console.WriteLine("Estos son las plantas a la venta");
                    {
                        foreach (Plantas planta in plantas)
                        {
                            Console.WriteLine(planta.Informacion());
                            Console.WriteLine(planta.InformacionS());
                        }
                        Console.WriteLine("Seleccione un tipo de planta: (6,7,8,9,10)");
                        decision = Console.ReadLine();
                        decisionN = Int32.Parse(decision);
                        Console.WriteLine("¿La Semilla o Crecida?(S/C)");
                        decision = Console.ReadLine();
                        foreach (Plantas planta in plantas)
                        {
                            if (dinero >= planta.Coste() && decision == "C")
                            {

                                granjaP.Add(plantas[decisionN - 6]);
                                dinero = dinero - planta.Coste();
                            }
                            else if (dinero >= planta.CosteS() && decision == "S")
                            {
                                granjaP.Add(plantas[decisionN - 6]);
                                dinero = dinero - planta.CosteS();
                            }
                            else
                            {
                                Console.WriteLine("No tiene suficiente dinero, lo sentimos");
                            }
                            Console.WriteLine("Gracias por su compra");
                        }

                    }
                }


                else if (decision == "3")
                {
                    Console.WriteLine($"Este es el precio actual de la expansión: {expansion}");

                    if (dinero > expansion)
                    {
                        expansion = expansion + 10 * expansiones;
                        expansion += 1;
                        Console.WriteLine($"Comprado. Tienes un terreno más grande");
                    }
                }
                else if (decision == "4")
                {

                }

                else if (decision == "5")
                {
                    Console.WriteLine("Antes de terminar el día. Le presento los disponibles para recolectar para mañana.");
                    foreach (Animales animales1 in animales)
                    {
                        if (animales1.EstadoRecoleccion() == true)
                        {

                            Console.WriteLine($"Recolectable: { animales1}");
                        }
                    }
                    foreach (Plantas plantas1 in plantas)
                    {
                        if (plantas1.EstadoRecoleccion() == true)
                        {

                            Console.WriteLine($"Recolectable: {plantas}");
                        }
                    }
                    Console.WriteLine("Que tengas dulces sueños!");
                }
                Console.ReadLine();
            }
        }
    }
}

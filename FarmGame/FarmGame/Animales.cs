﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmGame
{
    class Animales : Interface1, Interface2,Interface3,Interface4
    {
        private string nombre;
        private float precio;
        private float vida;
        private float productos;
        private bool estadoR;
        private bool estadoV;
        public Animales(string nombre,float precio, float vida, float productos, bool estadoR, bool estadoV) 
        {
            this.nombre = nombre;
            this.precio = precio;
            this.vida = vida;
            this.productos = productos;
            this.estadoR = estadoR;
            this.estadoV = estadoV;
        }

        public bool EstadoRecoleccion()
        {
            return estadoR;
        }
        public bool Supervivencia()
        {
            return estadoV;
        }
        public string Informacion()
        {
            return $"Nombre: {nombre} ,Precio: {precio}";
        }
        public string InformacionS()
        {
            return "";
        }
        public float Coste()
        {
            return precio;
        }
        public float CosteS()
        {
            return 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmGame
{
    abstract class Granja
    {
        protected Animales animales;
        protected Plantas plantas;

        protected Granja(Animales animales, Plantas plantas)
        {
            this.animales = animales;
            this.plantas = plantas;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmGame
{
    class Plantas : Interface1, Interface2, Interface3, Interface4
    {
        private string nombre;
        private float vida;
        private float frutos;
        private float preciosemilla;
        private float preciocrecido;
        private bool estadoR;
        private bool estadoV;
        public Plantas(string nombre, float vida,float frutos,float preciosemilla, float preciocrecido,bool estadoR,bool estadoV)
        {
            this.nombre = nombre;
            this.vida = vida;
            this.frutos = frutos;
            this.preciosemilla = preciosemilla;
            this.preciocrecido = preciocrecido;
            this.estadoR = estadoR;
            this.estadoV = estadoV;
        }

        public bool EstadoRecoleccion()
        {
            return estadoR;
        }
        public bool Supervivencia()
        {
            return estadoV;
        }
        public string InformacionS()
        {
            return $"Nombre: {nombre} ,Precio Semilla: {preciosemilla}";
        }
        public string Informacion()
        {
            return $"Nombre: {nombre} ,Precio Crecido: {preciocrecido}";
        }
        public float Coste()
        {
            return preciocrecido;
        }
        public float CosteS()
        {
            return preciosemilla;
        }
    }
}
